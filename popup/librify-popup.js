function reportExecuteScriptError(error) {
    document.querySelector("#popup-content").classList.add("hidden");
    document.querySelector("#error-content").classList.remove("hidden");
    console.error(`Failed to execute beastify content script: ${error.message}`);
}

var librifyBtn = document.getElementById('librifyPage');
librifyBtn.onclick = function(){librifyPage()}

function librifyPage() {
  // browser.tabs.executeScript({file: "../pages/dailykos.com.js"})
  // .then(listenForClicks)
  // .catch(reportExecuteScriptError);
  console.log("Librifying..");
}
