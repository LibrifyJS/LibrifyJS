/**
 *
 *    @licstart  The following is the entire license notice for the
 *    JavaScript code in this page.
 *
 *
 *       Copyright (C) 2018 Aurélio A. Heckert, Vinícius Melo
 *
 *   The JavaScript code in this page is free software: you can
 *   redistribute it and/or modify it under the terms of the GNU
 *   General Public License (GNU GPL) as published by the Free Software
 *   Foundation, either version 3 of the License, or (at your option)
 *   any later version.  The code is distributed WITHOUT ANY WARRANTY;
 *   without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.
 *
 *   As additional permission under GNU GPL version 3 section 7, you
 *   may distribute non-source (e.g., minimized or compacted) forms of
 *   that code without the copy of the GNU GPL normally required by
 *   section 4, provided you include this license notice and a URL
 *   through which recipients can access the Corresponding Source.
 *
 *
 *
 *   @licend  The above is the entire license notice
 *   for the JavaScript code in this page.
 *
 *
 *
 */

try {

  if (document.location.host === "www.dailykos.com") {
    console.log("Inside DailyKos");
    var script = document.createElement("script");
    script.onerror = (err)=> console.log("deu bosta: ", err);
    script.type = "text/javascript";
    script.src = "https://librifyjs.gitlab.io/LibrifyJS/dailykos.com.js";
    document.body.appendChild(script);
  }

} catch(err){
  console.log("It fails:", err)
}
