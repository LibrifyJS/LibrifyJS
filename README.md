# LibrifyJS
##  Make proprietary sites to work with LibreJS

That's a plugin maintained for Firefox with LibreJS active

LibrifyJS rewrites the proprietary code of websites, so it can be free!

In this version it only works with petitions in dailykos.com, but later the idea is to add other pages,
so we can make other proprietary websites to work with LibreJS.

Developers: [Aurélio A. Heckert](https://gitlab.com/aurium), [Vinícius Melo](https://gitlab.com/vinimlo)
